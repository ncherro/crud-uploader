$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "crud_uploader/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "crud_uploader"
  s.version     = CrudUploader::VERSION
  s.authors     = ["Nick Herro"]
  s.email       = ["ncherro@gmail.com"]
  s.homepage    = "http://www.ncherro.com"
  s.summary     = "Uses SimpleForm, CarrierWave and Plupload to provide easy upload functionality"
  s.description = "Uses SimpleForm, CarrierWave and Plupload to provide easy upload functionality"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", ">= 3.2.12"
  s.add_dependency "simple_form"
  s.add_dependency "carrierwave", "~> 0.7.0"
  s.add_dependency "flash_cookie_session"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "capybara"
  s.add_development_dependency "factory_girl_rails"
end
