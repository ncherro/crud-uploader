/* PLUPLOADER */
(function($, window, document, undefined) {
  var Pluploader = function(el, opts, i) {
    var settings = $.extend({}, $.fn.pluploader.defaults, opts),
      $el = $(el), uploader, container_id, $button, button_id, $file_input,
      $log, log_id, $img_wrap, img_wrap_id, data, $cached,
      progress_bar_html = '<div class="plup-progress"><div class="plup-progress-bar" style="width: 0%;"></div></div>';

    function init() {
      // set up the DOM
      // NOTE: we need to override the container id
      container_id = 'pluploader-' + i;
      $el.attr('id', container_id);
      button_id = container_id + '-button';
      log_id = container_id + '-log';
      progress_bar_id = container_id + '-progress_bar';
      // replace file input with button
      $file_input = $el.find('input:file');
      upload_path = $file_input.data('upload-path');
      $file_input.replaceWith('<a class="button" id="' + button_id + '" href="#">' + settings.button_text + '</a>')
      $button = $('#' + button_id);
      $img_wrap = $el.find(settings.img_wrap_sel);
      // set up our logger
      $log = $('<div id="' + log_id + '"></div>');
      $log.insertAfter($button);

      if (settings.cached_sel) $cached = $el.find(settings.cached_sel);

      // set up the pluploader
      uploader = new plupload.Uploader({
        runtimes: settings.runtimes,
        container: container_id,
        browse_button: button_id,
        max_file_size: settings.max_file_size,
        url: upload_path,
        flash_swf_url: settings.flash_swf_url,
        urlstream_upload: true,
        silverlight_xap_url: settings.silverlight_xap_url,
        multipart: true,
        multipart_params: settings.multipart_params,
        filters: settings.filters
      });
      // set up listeners
      if (settings.auto_start) uploader.bind('QueueChanged', plup_start); // auto start
      //uploader.bind('Init', plup_init);
      uploader.bind('FilesAdded', plup_added);
      uploader.bind('UploadProgress', plup_progress);
      uploader.bind('Error', plup_error);
      uploader.bind('FileUploaded', plup_uploaded);
      // initialize it
      uploader.init();
    }
    function plup_added(up, files) {
      $.each(files, function(i, file) {
        $log.append('<div id="' + file.id + '">Uploading ' + file.name + '... <b></b>' + progress_bar_html + '</div>');
      });
      up.refresh();
    }
    function plup_progress(up, file) {
      $('#' + file.id + ' .plup-progress-bar').css('width', file.percent + '%');
    }
    function plup_error(up, err) {
      if (err.file) {
        $('#' + err.file.id + ' b').html(err.message);
      } else {
        $log.append('Error: ' + err.message);
      }
    }
    function plup_uploaded(up, file, info) {
      $('#' + file.id).find('b').html('Uploaded!').end().delay(2000).fadeOut(function() { $(this).remove(); });
      data = $.parseJSON(info['response']);
      $img_wrap.html(data.image_tag);
      if ($cached) $cached.val(data.cached);
    }

    function plup_start() {
      uploader.start();
    }

    init();

  }, i=0;
  $.fn.pluploader = function(options) {
    return this.each(function(idx, el) {
      var $el = $(this), key = 'pluploader';
      if ($el.data(key)) { return; }
      i++;
      var pluploader = new Pluploader(this, options, i);
      $el.data(key, pluploader);
    });
  };
  $.fn.pluploader.defaults = {
    runtimes: 'html5,gears,silverlight,browserplus,flash',
    max_file_size: '10mb',
    button_text: 'Browse',
    img_wrap_sel: '.cu-img-wrap',
    auto_start: true,
    cached_sel: '.file-cached',
    filters: [
      { title: "Image files", extensions: "jpg,jpeg,gif,png" }
    ]
  };
})(jQuery, window, document);
