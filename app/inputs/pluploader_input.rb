class PluploaderInput < SimpleForm::Inputs::Base

  def additional_classes
    [input_type, required_class, readonly_class, disabled_class, 'clearfix'].compact
  end

  def input
    output = ""
    thumbnail_style = input_options[:thumbnail_style] || nil
    tmp_uploader = input_options[:tmp_uploader] || nil
    is_tmp = false
    if thumbnail_style
      if object.send("#{attribute_name}_tmp?")
        tmp_src = object.send("#{attribute_name}_tmp")
        if tmp_uploader
          # we are waiting for something to process - show it
          is_tmp = true
          img = %(<img src="/#{tmp_uploader.new.cache_dir}/#{tmp_src}" />)
        else
          img = %(<span class="processing">processing #{tmp_src.split('/').last}</span>)
        end
      else
        img = %(<img src="#{object.send(attribute_name).send(thumbnail_style)}" />) if object.send("#{attribute_name}?")
      end
      output = %(<div class="cu-img-wrap#{' tmp' if is_tmp}">#{img}</div>)
      if object.send("#{attribute_name}?")
        output << %(<label class="remove">#{@builder.check_box("remove_#{attribute_name}")} remove the image</label>)
      end
    else
      raise "A pluploader input is missing the required 'thumbnail_style' option."
    end
    output << %(<div class="cu-plupload-container">)
    input_html_options[:data] ||= {}
    input_html_options[:data][:upload_path] = input_options[:upload_path] || ''
    # add the file field
    output << @builder.file_field(attribute_name, input_html_options)
    # and add the cache field, which will hold the uploaded file and persist
    # between submissions in case there's an error on another field
    output << @builder.hidden_field("#{attribute_name}_cache", class: "file-cached hidden")
    output << %(</div>)
    output.html_safe
  end

end
