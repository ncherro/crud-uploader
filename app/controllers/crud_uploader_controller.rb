class CrudUploaderController < CrudUploader.parent_controller.constantize

  def image_was_processed
    if request.xhr?
      model = params[:model].constantize
      field = params[:field]
      obj = model.find_by_id(params[:id])
      style = params[:style] || 'thumb'
      if obj.send("#{field}_tmp?")
        render :nothing => true, :status => 404
      else
        render :json => {
          :src => obj.send(field).send(style).url,
          :full_src => obj.send(field).url,
        }
      end
    else
      render :text => 'not authorized', :status => 403
    end
  end

  protected
  def crud_upload_file(uploader_class, opts={})
    defaults = {
      render_view: true,
      thumbnail_style: 'admin_thumb',
    }
    opts = defaults.merge(opts)
    if params.has_key?(:file)
      uploader = uploader_class.new
      uploader.cache!(params[:file])
      @image = {
        :thumb_url => uploader.send(opts[:thumbnail_style]).url,
        :cached => uploader.url.split('/' + uploader_class.cache_dir + '/').last,
      }
      render :template => 'crud_uploader/crud_upload_file', :layout => false and return if opts[:render_view]
    end
  end

end
