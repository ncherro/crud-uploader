# CRUD Uploader

An opinionated image uploader for your Rails app.

Provides a base controller for your upload handler, a SimpleForm
pluploader input, and javascript to bring Plupload into the mix.

- SimpleForm for file inputs
- Plupload for file uploads
- CarrierWave for image processing and file storage


## Installation

Add this line to your gemfile

```
gem 'crud_uploader'
```

Then `bundle update` and add this line to the top of your `application.js`
manifest (or whichever file you're loading on the pages that need to
display image uploaders)

```
//= require crud_uploader
```

Now, we need to pass some settings from Rails to Plupload, so you'll
need to render a template that writes a javascript variable inline to
your page.

__Important__ render this _before_ you render your js manifest file

```
<%= render 'crud_uploader/settings' %>
```

## Usage

Here's an example of using our pluploader input type

```
<%= f.input :image, :as => :pluploader,
    :upload_path => admin_upload_image_path(:format => 'json'),
    :thumbnail_method => 'admin_thumb' %>
```

- `:upload_path` (required) defines the path to our upload handler
- `:thumbnail_method` (optional) defines the method we use to generate the
thumbnail we want to disply in the form. If you set a value, your model
must must respond to the method.

### Optional

You may also add this to the top of your css manifest file to use
default styles

```
*= require crud_uploader
```


## License

This project uses the MIT-LICENSE.
